import java.lang.Math;
import java.lang.Object;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Vector;
import java.util.Iterator;

/* 4 Late days */

public class DV implements RoutingAlgorithm {

    static int LOCAL = -1;
    static int UNKNOWN = -2;
    static int INFINITY = 60;

    /* update interval */
    int update_interval = 0;

    /* flags */
    boolean allow_preverse = false;
    boolean allow_expire = false;

    /* router */
    Router router = null;

    /* routing table */
    ArrayList<DVRoutingTableEntry> routing_table = null;

    public DV()
    {
      /* Initialize routing table when the class is constructed */
      this.routing_table = new ArrayList<DVRoutingTableEntry>();
    }

    public void setRouterObject(Router obj)
    {
      /* Assign obj to class variable router */
      this.router = obj;
    }

    public void setUpdateInterval(int u)
    {
      /* Set update interval */
      this.update_interval = u;
    }

    public void setAllowPReverse(boolean flag)
    {
      /* Set flag for Preverse */
      this.allow_preverse = flag;
    }

    public void setAllowExpire(boolean flag)
    {
      /*Set flag for expiration */
      this.allow_expire = flag;
    }

    public void initalise()
    {
      /* Safety checks */
      if (router == null) {
        //System.out.println("Router is null!");
      } else {
          /* Map router id to a LOCAL value and add to DVRoutingTable. */
          int id = this.router.getId();
          DVRoutingTableEntry dv_entry = new DVRoutingTableEntry(id, LOCAL, 0, this.update_interval);
          routing_table.add(dv_entry);
      }
    }

    public int getNextHop(int destination)
    {
      /* Iterate through whole routing table to find interface
      that the packet needs to go through to reach the requested destination */
      for (DVRoutingTableEntry entry : this.routing_table) {
        /* Get destination of routing table entry */
        int dest = entry.getDestination();
        /* If entry from routing table matches the requested destination,
        return the interface that the packet has to go through to reach the requested destination */
        if (dest == destination) {
          /* Returns outgoing interface of destination or local */
          return entry.getInterface();
        }
      }
      /* Return if the destination address is unknown */
      return UNKNOWN;
    }

    /* Expire routing table entries */
    private void expire_entries() {
      /* Go through the whole routing table to find entries that are invalid and expired */
      for(Iterator<DVRoutingTableEntry> it = this.routing_table.iterator(); it.hasNext();) {
        /* Get next entry */
        DVRoutingTableEntry entry = it.next();
        if (entry.getTime() == 0 && entry.getInterface() != LOCAL && entry.getMetric() == INFINITY) {
          /* Remove expired entry */
          it.remove();
        }
      }
    }

    /* Set all destination that go through a link that is down as INFINITY */
    private void mark_down() {
      /* Get Number of interfaces on this router */
      int ifaces = this.router.getNumInterfaces();
      /* Go through all interface */
      for (int i=0; i<ifaces; i++) {
        /* Check for interface's state: false - down; true - up */
        if (this.router.getInterfaceState(i) == false) {
          /* Go through the whole routing table to mark all entries that go through interface i as down (mark with value INFINITY) */
          for (DVRoutingTableEntry entry : this.routing_table) {
            /* Mark interface with i as down (INFINITY) that hasnt been marked yet, to avoid resetting the garbage timer */
            if (entry.getInterface() == i && entry.getMetric() != INFINITY) {
              /* Set entry metric value as INFINITY */
              entry.setMetric(INFINITY);
              /* Garbage collector timer */
              entry.setTime(this.update_interval * 2);
            }
          }
        }
      }
    }

    /* Count down timer */
    private void count_down() {
      for (DVRoutingTableEntry entry : this.routing_table) {
        /* Avoid getting below 0 and do not decrement the counter of non-INFINITY entries */
        if (entry.getTime() > 0 && entry.getMetric() == INFINITY && entry.getInterface() != LOCAL) {
          /* Get entry time */
          int time = entry.getTime();
          /* decrement time by 1 */
          entry.setTime(time - 1);
        }
      }
    }

    public void tidyTable()
    {

      /* Count down */
      count_down();

      /* Tidy table only if allow expire is true */
      if (this.allow_expire == true) {
        expire_entries();
      }

      /* Mark links that are down in the routing table */
      mark_down();
    }

    /* Copy Entry packet - avoid referencing problems in Java */
    private DVRoutingTableEntry copy_entry (DVRoutingTableEntry entry) {
      /* Get destination of the entry */
      int dest = entry.getDestination();
      /* Get interface of the entry */
      int iface = entry.getInterface();
      /* Get metric of the entry */
      int metric = entry.getMetric();

      /* Create a new routing table entry */
      DVRoutingTableEntry entry_copy = new DVRoutingTableEntry(dest, iface, metric, this.update_interval);

      return entry_copy;
    }

    /* Get router that is connected to this specific interface. */
    private int interface_map_router(int iface) {
      /* Get all links from router */
      Link[] links = this.router.getLinks();
      /* Get current router id as default*/
      int router_id = this.router.getId();

      /* Go through all the links to find the direct neighbor of this specific interface. */
      for (int i=0; i<links.length; i++) {
        /* If link with the outgoing interface matches the interface that is requested. */
        if (links[i].getInterface(0) == iface){
          /* Get router id that id directly connected to this specfic router's interface. */
          router_id = links[i].getRouter(0);
          break;
        }
      }
      /* Return router id that is directly connected to this specific router's interface .*/
      return router_id;
    }

    public Packet generateRoutingPacket(int iface)
    {
      /* Dont send packet if the interface's state is down. */
      if (this.router.getInterfaceState(iface) == false) {
        return null;
      }
        /* Send routing table entries to all immediate neighbors. */
        Payload payload = new Payload();
        /* Add the whole routing table into payload. */
        for (DVRoutingTableEntry entry : this.routing_table) {
          DVRoutingTableEntry sent_entry = copy_entry(entry);
          /* Split Horizon and Poison-Reverse optimization. */
          if (this.allow_preverse == true) {
            /* Get Router that is directy connected to the interface (1-hop distance). */
            int next_router = interface_map_router(iface);
            if (sent_entry.getDestination() != next_router) {
              /* All destinations that goes over this route is set to infinity. */
              if (sent_entry.getInterface() == iface) {
                sent_entry.setMetric(INFINITY);
              }
            } else if (sent_entry.getDestination() == this.router.getId()) {
              /* If the entry to be sent is this router's id then its metric will be set to 0
              so the metric calculation will start from 0 when they are added together. */
              if (sent_entry.getInterface() == LOCAL) {
                sent_entry.setMetric(0);
              }
            }
            /* add routing table entry into payload */
            payload.addEntry(sent_entry);
          } else {
            /* If the entry to be sent is this router's id then its metric will be set to 0
            so the metric calculation will start from 0 when they are added together. */
            if (sent_entry.getMetric() == LOCAL) {
              /* Set metric to 0 before sending */
              sent_entry.setMetric(0);
            }
            /* add routing table entry into payload. */
            payload.addEntry(sent_entry);
          }
        }
        /* Construct routing packet and add payload. */
        Packet p = new RoutingPacket(this.router.getId(), Packet.BROADCAST);
        /* Add payload to packet */
        p.setPayload(payload);
        return p;
    }

    public void processRoutingPacket(Packet p, int iface)
    {
      //System.out.println("Router " + this.router.getId() + " sending through interface " + iface);
      //System.out.println("Process Routing Packet");
      /* Get packet type. */
      int packet_type = p.getType();

      if (packet_type == Packet.ROUTING) {
        /* Process routing type packet. */
        //System.out.println("ROUTING PACKET");
        /* process routing type packet. */
        process_all_entries(p, iface);
      } else if (packet_type == Packet.BROADCAST) {
        /* Send through all of this router's interface. */
        //System.out.println("BROADCAST PACKET");
      } else if (packet_type == Packet.DATA) {
        /* Process data type packet. */
        //System.out.println("DATA PACKET");
      } else if (packet_type == Packet.UNKNOWN) {
        /* Process unknown packet. */
        //System.out.println("UNKNOWN PACKET");
      } else if (packet_type == Packet.UNKNOWNADDR) {
        /* Process packets with unknown address s. */
        //System.out.println("UNKNOWNADDR PACKET");
      } else {
        System.out.println("Something unexpected happened.");
      }
    }

    /* Extract data from packet and process each of them. */
    private void process_all_entries(Packet p, int iface) {
      /* Process all routing table entries. */
      Payload payload = p.getPayload();
      /* Get data from payload */
      Vector<Object> v = payload.getData();
      /* Transform data from Vector<Object> to Object[]. */
      Object[] routing_entries = v.toArray();

      /* Go through all objects and cast DVRoutingTableEntry to access data. */
      for (int i=0; i<routing_entries.length; i++) {
        DVRoutingTableEntry entry = (DVRoutingTableEntry)routing_entries[i];
        /*Process an entry */
        process_entry(entry, iface);
      }
    }

    /* Process an entry from the packet. */
    private void process_entry(DVRoutingTableEntry entry, int iface) {
      /* Process routing table entry. */
      int metric = entry.getMetric() + this.router.getInterfaceWeight(iface);
      /* Making sure that nothing can exceed INFINITY. */
      if (metric > INFINITY) {
        metric = INFINITY;
      }

      /* Look up for entry in routing table */
      DVRoutingTableEntry r = entry_lookup(entry.getDestination());
      /* Check if entry exists. */
      if (r == null) {
        /* Create new entry */
        DVRoutingTableEntry new_entry = new DVRoutingTableEntry(entry.getDestination(), iface, metric, this.update_interval);
        /* Add new entry into table */
        this.routing_table.add(new_entry);
      }
      /* Check if recorded interface is the same as the interface from which the packet came from. */
      else if (iface == r.getInterface()) {
        if (r.getMetric() != INFINITY) {
          /* Set new metric value */
          r.setMetric(metric);
          if (metric == INFINITY) {
            /* Set garbage timer */
            r.setTime(this.update_interval * 2);
          } else {
            /* Set update timer */
            r.setTime(this.update_interval);
          }
        } else {
          if (metric != INFINITY) {
            r.setMetric(metric);
            r.setTime(this.update_interval);
          }
        }
      }
      /* Check if recorded metric is greater than newly calculated metric. */
      else if (metric < r.getMetric()) {
        /* Update entry with new metric and interface from which the packet came from. */
        r.setMetric(metric);
        r.setInterface(iface);
      }
    }

    /* Lookup with the requested entry
    with the specific destination address exists in the routing table. */
    private DVRoutingTableEntry entry_lookup(int dest) {
      /* Look for destination entry in routing table */
      for (DVRoutingTableEntry entry : this.routing_table) {
        /* Entry with specific destination address exists */
        if (entry.getDestination() == dest) {
          /* Return routing table entry. */
          return entry;
        }
      }
      /* Return nothing. */
      return null;
    }

    /**
    Show Routing table.
    **/
    public void showRoutes() {
      /* Check for null pointer */
      if (routing_table != null && router != null) {
        /* Iterate through list to show routing table */
        System.out.println("Router " + this.router.getId());
        for (DVRoutingTableEntry entry : this.routing_table) {
          System.out.println(entry.toString());
        }
      }
    }
}

class DVRoutingTableEntry implements RoutingTableEntry
{
  private int dest;
  private int iface;
  private int metric;
  private int time;

    public DVRoutingTableEntry(int d, int i, int m, int t)
	{
    this.dest = d;
    this.iface = i;
    this.metric = m;
    this.time = t;

	}
    public int getDestination() { return this.dest; }
    public void setDestination(int d) { this.dest = d; }
    public int getInterface() { return this.iface; }
    public void setInterface(int i) { this.iface = i; }
    public int getMetric() { return this.metric; }
    public void setMetric(int m) { this.metric = m; }
    public int getTime() {return this.time; }
    public void setTime(int t) { this.time = t; }

    public String toString()
	{
	    return "d " + this.dest + " i " + this.iface + " m " + this.metric;
	}
}
